from setuptools import setup, find_packages

setup(
    name='jingpad-encoder',
    version='0.0.1',
    author='Francesco Noacco',
    url='https://gitlab.com/noaccOS/jingpad-video-encoder/',
    packages=find_packages(),#, 'jingpad-encoder.*']),
    install_requires=['pymediainfo'],
    entry_points= {
        'console_scripts': ['jingpad-encoder=encoder.cli:main'],
    },
)
