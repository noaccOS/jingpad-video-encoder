# JingPad Video Encoder

Project to re-encode all the videos in a directory to a format playable by the JingPad.

This release is tailored to my needs so it has dubious defaults
- Always pick Japanese audio
- Always pick English subtitles (avoiding Signs & Songs)
- Default speed to 2
- Weird root for the extracted files

The program still asks for a prompt before picking audio and subs tracks so it's possible to input another value that way
(or using the appropriate argument flags).

> Note that the track id should be the one shown before the prompt, which is the same output from `mediainfo`. Do not use the track used by `mkvextract`, it will be adjusted automatically.

All the files will be placed within a directory called the _jingpad root_, and then inside a subdirectory (possibly a subdirectory structure) inside it (_media directory_)

## Functions
Here's a list of all the available functions. For the planned features, check the issues.
- Re encode `mkv` files to an `mp4`, `x264`, 8bit color space file
- Hardcode subtitles to make them visible in the ubuntu touch player
- Choose one audio track to keep. You don't need more than one anyway
- Automatic selection of both subs and audio tracks (anime biased)
- Change default playback speed (also changes the fps), up to 2 (because of `ffmpeg`'s `atempo` limitations, it needs work to go higher)
- Possibility to use (nvidia) gpu for encoding
- Rename the files using the first group of a regex
    e.g. 
    - Title: `Non Non Biyori - 01.mkv`
    - Regex: `(\d\d)\.mkv`
    - New name: `01`

You can always use `--help` for a list of the options.

## Usage
If you use `nix`, you can just `nix-env -if .` from the cloned directory and it should be installed. 

If you use something else, you need
- `mkvtoolnix-cli`
- A newish version of `ffmpeg` (it needs to support `source_fps`)
- `python3` with `pymediainfo`.

Here is a simple example use
```shell
Anime/Non Non Biyori (Season 01+02+03+OVA+Movie)/Non Non Biyori
❯ ls -1
Non Non Biyori - 01.mkv
Non Non Biyori - 02.mkv
Non Non Biyori - 03.mkv
Non Non Biyori - 04.mkv
Non Non Biyori - 05.mkv
Non Non Biyori - 06.mkv
Non Non Biyori - 07.mkv
Non Non Biyori - 08.mkv
Non Non Biyori - 09.mkv
Non Non Biyori - 10.mkv
Non Non Biyori - 11.mkv
Non Non Biyori - 12.mkv
Non Non Biyori - OVA.mkv

Anime/Non Non Biyori (Season 01+02+03+OVA+Movie)/Non Non Biyori
❯ jingpad-encoder -d 'Non Non Biyori/S1' -r '(\d\d)\.mkv'
```
> Warning: with this regex, the OVA won't match and it won't be re-encoded. If you want to also match the ova, you should use `-r '(\d\d|OVA)\.mkv'` instead
