def track_from_id(tracks, track_id):
    return next(filter(lambda x: x.track_id == track_id, tracks), None)
