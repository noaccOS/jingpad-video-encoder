#!/usr/bin/env python3

import argparse
import os
from pathlib import Path
from encoder.core import load, encode, sorted_files
from encoder.data import Group
from encoder.helper import track_from_id

def parse_arguments():
    parser = argparse.ArgumentParser(description='ffmpeg wrapper jingpad-compatible')
    parser.add_argument('wd', nargs='?', default='.', type=Path, help='Woring directory of the script, containing the files to re-encode')
    parser.add_argument('--audio-id', '-a', default=None, help='Audio track. Select automatically when not given')
    parser.add_argument('--subtitle-id', '-s', default=None, help='Subtitle track. Select automatically when not given')
    parser.add_argument('--jingpad-root', '-j', default='/home/media/jingpad/', help='Root directory for script output')
    parser.add_argument('--media-directory', '-d', default=None, help='Output directory name. Use the directory name as default')
    parser.add_argument('--use-gpu', '-g', action='store_true', help='Whether to use nvenc encoding. Defaults to using only cpu')
    parser.add_argument('--rename', '-r', default=None, help='Rename the files by keeping only the group from a specified regex.')
    parser.add_argument('--sub-dir', '-S', default='.subs', help='Extracted subtitle directory name')
    parser.add_argument('--speed', default=2, help='how much faster do you want the output? yes')
    parser.add_argument('--no-auto', action='store_true', help='Never choose tracks without confirmation')
    parser.add_argument('--no-subs', action='store_true', help='Don\'t include (hardcoded) subs in the output')
    return parser.parse_args()

def make_input(title, tracks, guess):
    print(title)
    print('\n'.join([str(t) for t in tracks]))
    print()
    
    guess_track = str(guess.track_id) if guess is not None else None
    selected = None
    input_str = "Track"+\
                (f" [{guess_track}]" if guess_track is not None else '') +\
                ": "
    track_ids = [str(x.track_id) for x in tracks]
    while not selected:
        selected = input(input_str) or guess_track
        if selected not in track_ids:
            print(f'{selected}: invalid choice\n')
            selected = None

    # Error handling not required as selected must be a track id for the loop to finish
    return int(selected)

def main():
    args = parse_arguments()
    
    wd = args.wd if args.wd.is_dir else args.wd.parent
    outdir = Path(args.jingpad_root).joinpath(args.media_directory or os.path.basename(wd)).resolve()
    subspath = outdir.joinpath(args.sub_dir)
    subspath.mkdir(parents=True,exist_ok=True)

    groups = load(wd, args.rename)
    for group in groups:
        print(f'Group {group.index}/{Group._Index}')
        print(f'e.g.: {group.files[0]}')
        print()
        if args.audio_id:
            at = track_from_id(group.ats, args.audio_id)
            if at is None:
                print(f'Invalid audio id: {args.audio_id}')
                return 1
            print(f'Audio track: {at}')
        else:
            at = make_input('Audio tracks:', sorted(list(group.ats), key=lambda x: x.track_id), group.guess_audio())
        group.atrack = at
        if args.subtitle_id:
            st = track_from_id(group.sts, args.subtitle_id)
            if st is None:
                print(f'Invalid subtitle id: {args.subtitle_id}')
                return 1
            print(f'Subtitle track: {st}')
        elif args.no_subs:
            st = None
        else:
            st = make_input('Subtitle tracks:', sorted(list(group.sts), key=lambda x: x.track_id), group.guess_sub())
        group.strack = st

    for f,g in sorted_files(groups):
        encode(f.oldf,
               outdir.joinpath(f'{f.newf}.mp4'),
               subspath.joinpath(f.newf),
               g.atrack,
               g.strack,
               g.sformat,
               args.speed,
               args.use_gpu)

    return 0

if __name__ == '__main__':
    exit(main())
