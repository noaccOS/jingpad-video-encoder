from pymediainfo import MediaInfo
from pathlib import Path
from encoder.data import Group, File, ATrack, STrack
from typing import List
import os
import re

sformat_text = ('ASS')
sformat_image = ('PGS')

def subtitle_format_string(input_path: Path, sfile: Path, strack, sformat):
    if strack == -1:
        return "null"
    sfile = sfile.resolve()
    # ffmpeg/mkvextract id
    strack -= 1
    if sformat in sformat_text:
        os.system(f'/usr/bin/env mkvextract tracks "{input_path}" {strack}:"{sfile}"')
        return f'subtitles={sfile}'
    if sformat in sformat_image:
        return f'[0:{strack}]overlay'
    raise Exception(f'Unknown subtitle type. You can open an issue requesting the subtitle type "{sformat}".')

def encode(inputfile: Path, outputfile: Path, sfile: Path, atrack, strack, sformat, speed, usegpu):
    info = MediaInfo.parse(inputfile)
    os.system(f'/usr/bin/env ffmpeg -i "{inputfile.resolve()}" -filter_complex "[0:v]format=yuv420p[i];[i]{subtitle_format_string(inputfile, sfile, strack, sformat)}[j];' +
              f'[j]setpts=1/{speed}*PTS[k];[k]fps=fps=source_fps*{speed}[v]' +
              (f';[0:{atrack-1}]atempo={speed}[a]" -map "[a]"' if atrack is not None else '"') +
              f' -map "[v]" -c:v {"h264_nvenc" if usegpu else "libx264"} -t {info.general_tracks[0].duration/1000/speed} ' +
              f'"{outputfile.resolve()}"')

def preview(wd: Path, regex):
    newname = \
        (lambda x: x.stem)\
        if regex is None else\
        (lambda x: mch.group(1) if (mch := re.search(regex, x.name)) is not None else None)

    allfiles = sorted(list(wd.glob('*.mkv')))
    files = [File(x,y,MediaInfo.parse(x)) for x in allfiles if (y := newname(x)) is not None]

    return files

def load(wd: Path, regex):
    files = preview(wd, regex)
    
    groups = []
    for f in files:
        ats = {ATrack(x.track_id, x.language, x.title)           for x in f.mi.audio_tracks}
        sts = {STrack(x.track_id, x.language, x.title, x.format) for x in f.mi.text_tracks}

        g = next(filter(lambda x: x.ats == ats and x.sts == sts, groups), None)
        if g is None:
            g = Group(ats,sts)
            groups.append(g)
            
        g.files.append(f)
        
    return groups

def sorted_files(l: List[Group]):
    file_tuples = map(lambda g: zip(g.files, [g] * len(g.files)), l)
    file_tuples = [inner for outer in file_tuples for inner in outer]
    return sorted(file_tuples, key=lambda x: x[0].newf)
