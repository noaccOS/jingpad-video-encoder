from typing import List, Set
from encoder.helper import track_from_id

class File:
    def __init__(self, oldf, newf, mi):
        self.oldf = oldf
        self.newf = newf
        self.mi   = mi

    def __str__(self) -> str:
        return str(self.oldf)

    def __repr__(self) -> str:
        return self.__str__()

class ATrack:
    def __init__(self, track_id, language, title):
        self.track_id = track_id
        self.language = language
        self.title    = title

    def __hash__(self) -> int:
        return (self.track_id, self.language, self.title).__hash__()

    def __str__(self) -> str:
        return f'{self.track_id} - {self.language}: {self.title}'

    def __eq__(self, __o: object) -> bool:
        return str(self).__eq__(str(__o))

class STrack:
    def __init__(self, track_id, language, title, format):
        self.track_id = track_id
        self.language = language
        self.title    = title
        self.format   = format

    def __hash__(self) -> int:
        return (self.track_id, self.language, self.title, self.format).__hash__()

    def __str__(self) -> str:
        return f'{self.track_id} - {self.language}: {self.title} ({self.format})'
    
    def __eq__(self, __o: object) -> bool:
        return str(self).__eq__(str(__o))
    
class Group:
    _Index = 0
    
    def __init__(self, ats: Set[ATrack], sts: Set[STrack]):
        Group._Index += 1
        
        self.files: List[File] = []
        self.index = Group._Index
        self.ats = ats
        self.sts = sts
        self.atrack:   int | None = None
        self._strack:  int | None = None
        self._sformat: str | None = None

    @property
    def strack(self):
        return self._strack

    @property
    def sformat(self):
        return self._sformat

    @strack.setter
    def strack(self, value):
        self._strack = value
        track = track_from_id(self.sts, value)
        if track is None:
            raise Exception(f'Invalid subtitle track: {value}')
        self._sformat = track.format

    def guess_audio(self):
        if not self.ats:
            return None
    
        # If only one audio track, return that one
        if len(self.ats) == 1:
            return next(iter(self.ats))
    
        choice = None
        japs = [x for x in self.ats if x.language == 'ja']
        if len(japs) == 1:
            choice = japs[0]
        elif len(japs) > 1:
            notcommentary = [x for x in japs if 'commentary' not in x.title.lower()]
            if len(notcommentary) == 1:
                choice = notcommentary[0]
        else:
            engs = [x for x in self.ats if x.language == 'en']
            if len(engs) == 1:
                choice = engs[0]
            elif len(engs) > 1:
                notcommentary = [x for x in engs if 'commentary' not in x.title.lower()]
                if len(notcommentary) == 1:
                    choice = notcommentary[0]
    
        return choice
    
    def guess_sub(self):
        if not self.sts:
            return None
        
        if len(self.sts) == 1:
            return next(iter(self.sts))
    
        choice = None
        engs = [x for x in self.sts if x.language == 'en']
        if len(engs) == 1:
            choice = engs[0]
        elif len(engs) > 1:
            notss = [x for x in engs if all (word not in x.title.lower() for word in ('sign', 'song'))]
            choice = notss[0]
    
        return choice
