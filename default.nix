{pkgs ? import <nixpkgs> {}}:

pkgs.python3Packages.buildPythonApplication rec {
  pname = "jingpad-encoder";
  version = "0.0.1";

  src = ./.;

  pythonPath = with pkgs.python3Packages; [ pymediainfo setuptools ];

  patchPhase = ''
    substituteInPlace encoder/core.py \
      --replace "/usr/bin/env mkvextract" "${pkgs.mkvtoolnix-cli}/bin/mkvextract" \
      --replace "/usr/bin/env ffmpeg" "${pkgs.ffmpeg_5}/bin/ffmpeg"
  '';

  doCheck = false;
}
